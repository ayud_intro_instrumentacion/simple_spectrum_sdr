from argparse import ArgumentParser
import numpy as np
import adi
#import time
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
import scipy.signal as signal

if __name__ == "__main__":
    description="Estimacion de densidad de potencia"
    parser = ArgumentParser(conflict_handler="resolve", description=description)
    parser.add_argument("-i","--ip",required=True,type=str,\
        help="direccion IP del dispositivo SDR")
    parser.add_argument("-fc","--frecuencia", type=float,required=True,\
        help="frecuencia central en MHz")
    parser.add_argument("-s","--sample_rate",type=float, default=50e6,\
        help="frecuencia de muestreo en MHz, por defecto 50MHz")
    parser.add_argument("-n","--nfft", default=1024,\
         help="tamaño de la fft, por defecto 1024 (se recomienda una potencia de 2)")
    parser.add_argument("-g","--gain", default=30, type=float,\
        help="ganancia de recepción en dB, por defecto 30dB")
    args = parser.parse_args()

    ip = "ip:" + args.ip
    center_freq = args.frecuencia * 1e6
    sample_rate = args.sample_rate*1e6
    nfft = args.nfft
    gain = args.gain

    sdr = adi.ad9364(ip)
    sdr.sample_rate = int(sample_rate)
    sdr.rx_rf_bandwidth = int(sample_rate) #ancho de banda del filtro RF
    sdr.rx_lo = int(center_freq)
    sdr.rx_hardwaregain_chan0 = gain# solo valido si el gain control mode esta en manual
    sdr.gain_control_mode_chan0 = "manual"# manual, slow_rate, fast_rate
    #sdr._set_iio_attr('voltage0', 'hardwaregain', False, gain)
    sdr.rx_buffer_size = int(100000) #longitud del buffer de recepcion de muestras
    samples = sdr.rx() # activacion del SDR y obtención de muestras

    #==========================================================================
    # Calculo del espectro de potencia por el metodo de Welch
    #==========================================================================
    fs,psd = signal.welch(samples,fs=sample_rate,nperseg=nfft,\
                        return_onesided=False)
    psd = np.fft.fftshift(psd)
    fs = np.fft.fftshift(fs)+center_freq
    psd = 10*np.log10(psd)
    #print(sdr._get_iio_attr('voltage0','hardwaregain', False))

    #==========================================================================
    # Grafica del espectro de potencia
    #==========================================================================
    plotEngformat = EngFormatter(unit='Hz')
    plt.plot(fs,psd)
    plt.gca().xaxis.set_major_formatter(plotEngformat)
    plt.grid()
    plt.title('Espectro de Potencia')
    plt.ylabel('dB')
    plt.xlabel('Frecuencia')
    plt.show()
