# Simple Spectrum

## Descripción
Este programa permite capturar una señal RF mediante cualquier dispositivo SDR (_Software Defined Radio_) de **Analog Devices** y estimar su PSD (_Power Spectrum Density_).

## Instrucciones
Se recomienda tener instaldo Miiconda con python 3.8. Para instalarlo puede ir a [Instalación de Miniconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) y seguir los pasos para la instalación regular.

Luego, en una consola de `conda`, ir a la carpeta donde ha copiado el proyecto y ejecutar el comando:
```
conda env create -f environment_pysdr.yml
```
Esto creará un ambiente en `conda` llamado `pysdr` que contiene todos los paquetes de software necesarios para ejecutar el programa. Para activar el ambiente `pysdr` ejecute el comando:
```
conda activate pysdr
```
aparcerá al incio del promp de su consola el texto `(pysdr)`, indicando que ahora esta en en el ambiente `pysdr`. 
#### Uso del Programa
El programa `spectrum.py` recibe los siguientes argumentos:

- `-i` o `--ip`, dirección ip o usb según sea el caso del dispositivo SDR
- `fc` o `--frecuencia`, frecuencia en MHz central a la cual el dispositivo SDR se sintonizará
- `-s` o `--sample_rate`, frecuencia de muestreo en MHz, por defecto 50MHz
- `-n` o `--nfft`, longitud de la FFT (*Fast Fourier Transform*) utilizada para el cálculo del PSD, por defecto 1024
- `-g` o `--gain`, ganancia en dB del amplicador de recepción del dispositivo SDR, por defecto 30dB

Por ejemplo, en el ambiente `pysdr` y dentro de la carpeta del proyecto, puede ejecutar el programa con los siguientes parámetros (suponiendo que la dirección ip del dispositivo es 10.42.0.139):
```
python spectrum.py -i 10.42.0.139 -fc 882 -n 2048 -s 40 -g 33
```
esto generará una ventana como se muestra a continuación 
![PSD image](img/psd.png)

El comando anterior captura la señal en 882MHz (Downlink de GSM-850, señal celular) con una frecuencia de muestreo de 40MHz (que es igual al ancho de banda de la captura) y con una ganancia del amplificador de 33dB.
